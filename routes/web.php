<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/* @var $router \Laravel\Lumen\Routing\Router */
$router->group(['prefix' => '/author'], function($router){
    /* @var $router \Laravel\Lumen\Routing\Router */
    $router->get   ('/',       'Author@getList');
    $router->get   ('/{uuid}', 'Author@get');
    $router->post  ('/',       'Author@add');
    $router->put   ('/{uuid}', 'Author@update');
    $router->delete('/{uuid}', 'Author@remove');
    $router->get   ('/{uuid}/books', 'Author@getBooks');
    $router->post  ('/{uuid}/books', 'Author@addBooks');
});

$router->group(['prefix' => '/book'], function($router){
    /* @var $router \Laravel\Lumen\Routing\Router */
    $router->get   ('/',       'Book@getList');
    $router->get   ('/{uuid}', 'Book@get');
    $router->post  ('/',       'Book@add');
    $router->put   ('/{uuid}', 'Book@edit');
    $router->delete('/{uuid}', 'Book@remove');
});
