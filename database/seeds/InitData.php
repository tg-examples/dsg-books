<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class InitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = Uuid::uuid1();
        DB::table('authors')->insert([
            'id' => $id,
            'first_name' => 'Nicola',
            'last_name' => 'Tesla',
            'country' => 'Romania',
        ]);
        DB::table('books')->insert([
            'id' => Uuid::uuid1(),
            'author_id' => $id,
            'name' => 'A great discovery',
            'price' => 30.49,
            'pages' => 585,
        ]);
        DB::table('books')->insert([
            'id' => Uuid::uuid1(),
            'author_id' => $id,
            'name' => 'Electric fields',
            'price' => 28.55,
            'pages' => 328,
        ]);

        $id = Uuid::uuid1();
        DB::table('authors')->insert([
            'id' => $id,
            'first_name' => 'Mr',
            'last_name' => 'Bean',
            'country' => 'England',
        ]);
        DB::table('books')->insert([
            'id' => Uuid::uuid1(),
            'author_id' => $id,
            'name' => 'How to create a bean',
            'price' => 36.98,
            'pages' => 174,
        ]);

        $id = Uuid::uuid1();
        DB::table('authors')->insert([
            'id' => $id,
            'first_name' => 'Homer',
            'last_name' => 'Simpson',
            'country' => 'USA',
        ]);
        DB::table('books')->insert([
            'id' => Uuid::uuid1(),
            'author_id' => $id,
            'name' => 'My yellow world',
            'price' => 60.00,
            'pages' => 412,
        ]);
    }
}
