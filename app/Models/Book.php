<?php

namespace App\Models;

class Book extends Model {

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'books';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['id','name','price','pages'];

    /**
     * The attributes that should be hidden for serialization.
     * @var array
     */
    protected $hidden = ['author_id'];

    public function author(){
        return $this->belongsTo('App\\Models\\Author', 'author_id', 'id');
    }

}
