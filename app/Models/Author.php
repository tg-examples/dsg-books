<?php

namespace App\Models;

class Author extends Model {

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'authors';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['id','first_name','last_name','country'];

    public function books(){
        return $this->hasMany('App\\Models\\Book', 'author_id', 'id');
    }

}
