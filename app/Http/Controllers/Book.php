<?php

namespace App\Http\Controllers;

class Book extends Controller{

    protected $identifier = 'book';
    protected $identifierPlural = 'books';
    protected $source = 'Book';

    /**
     * Gets a single book with the author item from database
     * @param string $uuid
     * @param mixed $ignored
     * @return Response
     */
    public function get($uuid, $ignored = null){
        return parent::get($uuid, ['author']);
    }

    /**
     * Gets a list of book with authors from database
     * @param mixed $ignored
     * @return Response
     */
    public function getList($ignored = null){
        return parent::getList(['author']);
    }

}
