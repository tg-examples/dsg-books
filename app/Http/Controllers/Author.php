<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Ramsey\Uuid\Uuid;

class Author extends Controller{

    protected $identifier = 'author';
    protected $identifierPlural = 'authors';
    protected $source = 'Author';

    /**
     * Gets a books list written by the author
     * @param string $uuid
     * @return Response
     */
    public function getBooks($uuid){
        try {
            $author = $this->getSource()::findOrFail($uuid);
            return $this->response($author->books->toArray(),200,[],'books');
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while getting the {$this->identifier}",
                'error' => $ex->getMessage()
            ], 400);
        }
    }

    /**
     * Adds a new book written by the author
     * @param string $uuid
     * @return Response
     */
    public function addBooks($uuid){
        $data = $this->request->input();

        if(!Arr::has($data, 0)){
            $data = [$data];
        }
        foreach($data as &$item){
            $item['id'] = Uuid::uuid1();
        }

        try {
            $author = $this->getSource()::findOrFail($uuid);
            $author->books()->createMany($data);
            return $this->response(['msg' => "The books successfully added"]);
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while adding the books",
                'error' => $ex->getMessage()
            ], 400);
        }
    }
}
