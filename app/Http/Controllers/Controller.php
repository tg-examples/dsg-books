<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Lumen\Routing\Controller as BaseController;
use Ramsey\Uuid\Uuid;

abstract class Controller extends BaseController{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var string The identifier of the controller
     */
    protected $identifier = 'item';

    /**
     * @var string The plural identifier of the controller
     */
    protected $identifierPlural = 'items';

    /**
     * @var string The source name
     */
    protected $source = null;


    public function __construct(Request $request, Response $response) {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * Create a response type depends by HTTP content type
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return mixed
     */
    protected final function response(array $data, $status = 200, array $headers = [], $elementName = 'response')
    {
        $type = $this->request->getContentType();
        switch($type){
            case 'json': $respones = response()->json($data, $status, $headers); break;
            case 'xml': $respones = $this->response->xml($data, $status, $headers, $elementName); break;
            default: $respones = response($data, $status, $headers);
        }
        return $respones;
    }

    /**
     * Returns a source full name
     * @return string
     * @throws \Exception
     */
    protected function getSource(){
        if(!$this->source){
            throw new \Exception('The $source of data is not set');
        }
        return "\\App\\Models\\{$this->source}";
    }

    /**
     * Gets a single data item from database
     * @param string $uuid
     * @param array $resultWith
     * @return Response
     */
    public function get($uuid, $resultWith = []){
        try {
            $result = $this->getSource()::findOrFail($uuid);
            $resultWith && $result->load($resultWith);
            return $this->response($result->toArray(),200,[],$this->identifier);
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while getting the {$this->identifier}",
                'error' => $ex->getMessage()
            ], 400);
        }
    }

    /**
     * Gets a list of data items from database
     * @param array $resultWith
     * @return Response
     */
    public function getList($resultWith = []){
        try {
            $result = $this->getSource()::all();
            $resultWith && $result->load($resultWith);
            return $this->response($result->toArray(),200,[],$this->identifierPlural);
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while listing {$this->identifierPlural}",
                'error' => $ex->getMessage()
            ], 400);
        }
    }

    /**
     * Adds a new item or multiple items into database
     * @return Response
     */
    public function add(){
        $data = $this->request->input();

        if(!Arr::has($data, 0)){
            $data = [$data];
        }
        foreach($data as &$item){
            $item['id'] = Uuid::uuid1();
        }

        try {
            $this->getSource()::insert($data);
            return $this->response(['msg' => "The {$this->identifier} successfully added"]);
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while adding the {$this->identifier}",
                'error' => $ex->getMessage()
            ], 400);
        }
    }

    /**
     * Updates the data in database
     * @param string $uuid
     * @return Response
     */
    public function update($uuid){
        $data = $this->request->input();

        try {
            $source = $this->getSource()::findOrFail($uuid);
            $source->fill($data);
            $source->save();
            return $this->response(['msg' => "The {$this->identifier} successfully updated"]);
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while updating the {$this->identifier}",
                'error' => $ex->getMessage()
            ], 400);
        }
    }

    /**
     * Removes the data from database
     * @param string $uuid
     * @return Response
     */
    public function remove($uuid){
        try {
            $source = $this->getSource()::findOrFail($uuid);
            $source->delete();
            return $this->response(['msg' => "The {$this->identifier} successfully deleted"]);
        } catch (\Exception $ex) {
            return $this->response([
                'msg'   => "An error occurred while deleting the {$this->identifier}",
                'error' => $ex->getMessage()
            ], 400);
        }
    }

}
