# Aplikacja przykładowa na Lumen 5

## Instalacja
W celu instalacji projektu wykonać poniższe polecenia w konsoli
`git clone https://bitbucket.org/tg-examples/dsg-books.git books`
`cd books`
`composer run-script post-root-package-install`

## Konfiguracja
W pliku .env skonfigurować dane bazy danych
Następnie wykonać w konsoli polecenia
`php artisan migrate`
`php artisan db:seed --class=InitData`

## Uruchomienie
Wykonać z konsoli polecenie
`php -S localhost:8080 -t public`